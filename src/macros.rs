//! twisty macros
macro_rules! out(
    ($($args:tt)+) => {
        {
            use std::io::{self, Write};
            let stdout = io::stdout();
            let mut handle = stdout.lock();
            write!(handle, $($args)+).expect("Unable to write to stdout");
            handle.flush().expect("Unable to flush to stdout");
        }
    }
);

macro_rules! err(
    ($($args:tt)+) => {
        {
            use std::io::{self, Write};
            let stderr = io::stderr();
            let mut handle = stderr.lock();
            writeln!(handle, $($args)+).expect("Unable to write to stderr");
            handle.flush().expect("Unable to flush to stderr");
        }
    }
);
