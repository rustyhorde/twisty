//! tokio-proto client for twist
extern crate base64;
extern crate blake2;
extern crate byteorder;
extern crate clap;
extern crate env_logger;
extern crate futures;
extern crate native_tls;
extern crate rand;
extern crate sha1;
extern crate slog_stream;
extern crate slog_term;
extern crate term;
extern crate tokio_core;
extern crate tokio_proto;
extern crate tokio_service;
extern crate tokio_tls;
extern crate twist;
extern crate twist_lz4;
extern crate url;

#[macro_use]
extern crate slog;
#[macro_use]
mod macros;

mod client;
mod config;
mod error;
mod run;

use std::process;

/// twisty entry point
fn main() {
    process::exit(run::run(None));
}
